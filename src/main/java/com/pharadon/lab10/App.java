package com.pharadon.lab10;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec);
        System.out.println("Area ="+rec.calArea());
        System.out.print("Perimeter ="+rec.calPerimeter());
        System.out.println();
        System.out.println();

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2);
        System.out.println("Area ="+rec2.calArea());
        System.out.print("Perimeter ="+rec2.calPerimeter());
        System.out.println();
        System.out.println();

        Circle circle1 = new Circle(2);
        System.out.println(circle1);
        System.out.printf("%s area  %.3f \n",circle1.getName(), circle1.calArea());
        System.out.printf("%s perimeter %.3f \n",circle1.getName(),circle1.calPerimeter());  //printf = printformat
        System.out.println();

        Circle circle2 = new Circle(4);
        System.out.println(circle2);
        System.out.printf("%.3f \n", circle2.calArea());
        System.out.printf("%s perimeter %.3f \n",circle2.getName(),circle2.calPerimeter());
        System.out.println();

        Triangle triangle1 = new Triangle(2, 3, 4);
        System.out.println(triangle1);
        System.out.printf("%s area  %.3f \n",triangle1.getName(), triangle1.calArea());
        System.out.printf("%s area %.3f\n",triangle1.getName()  ,triangle1.calPerimeter());
        System.out.println();

        Triangle triangle2 = new Triangle(6, 9, 9);
        System.out.println(triangle2);
        System.out.printf("%s area  %.3f \n",triangle2.getName(), triangle2.calArea());
        System.out.printf("%s area %.3f\n",triangle2.getName()  ,triangle2.calPerimeter());

    }
}
