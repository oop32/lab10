package com.pharadon.lab10;

import java.util.HexFormat;

public class Rectangle extends Shape{
    private double width;
    private double height;
    public Rectangle(double width,double height){
        super("Rectangle");  // ใส่ชื่อ ตาม require ของ constructor Shape
        this.width = width;
        this.height = height;
    }
    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }
    @Override
    public String toString() {
        return this.getName() + " width: " +this.width +" height: " +this.height;
    }

    @Override
    public double calArea() {
        return this.width * this.height;
    }

    @Override
    public double calPerimeter() {
        return this.width*2 + this.height*2;
    }
}
