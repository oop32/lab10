package com.pharadon.lab10;

public abstract class Shape {
    private String name;

    public Shape(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract double calArea();  //ใส่ abstract เพราะว่ามันคำนวณไม่ได้ แต่จำเป็นต้องมีไว้ให้ class ลูกได้ extend ต่อ 

    public abstract double calPerimeter();
}
